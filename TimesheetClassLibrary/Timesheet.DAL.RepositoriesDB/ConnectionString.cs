﻿using System.Configuration;

namespace Timesheet.DAL.RepositoriesDB
{
    public class ConnectionString 
    {
        public string Connection = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
    }
}