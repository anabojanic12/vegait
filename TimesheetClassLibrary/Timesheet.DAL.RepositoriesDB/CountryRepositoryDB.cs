﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Contracts;
using Timesheet.Models;
using System.Configuration;

namespace Timesheet.DAL.RepositoriesDB
{
    public class CountryRepositoryDB : ICountryRepository
    {
        private readonly string _connection;

        public CountryRepositoryDB(string connectionString)
        {
            if(string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Connection string can't be empty", nameof(connectionString));
            }
            this._connection = connectionString;  
        }
      
        public Country FindCountryById(Guid id)
        { 
            using (SqlConnection conn = new SqlConnection(_connection))
            { 
                conn.Open();
                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = @"SELECT * FROM Country WHERE Id=@Id";
                    command.Parameters.Add(new SqlParameter("@Id", id));


                    using (SqlDataAdapter daClient = new SqlDataAdapter())
                    {
                        DataSet dsClient = new DataSet();
                        daClient.SelectCommand = command;
                        daClient.Fill(dsClient, "Country");

                        var reader = command.ExecuteReader();

                        if (reader.HasRows)
                        {
                            //while (reader.Read())
                            //{
                                int clientId = reader.GetInt32(1);
                                string clientName = reader.GetString(2);

                                return new Country(Guid.NewGuid(), clientName);
                                }
                            //}
                        }
                    }
            }
            return null;
        }


        public IEnumerable<Country> GetAll()
        {
            List<Country> countries = new List<Country>();
            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = @"SELECT * FROM Country";

                        using (SqlDataAdapter daCountry = new SqlDataAdapter())
                        {
                            DataSet dsCountry = new DataSet();
                            daCountry.SelectCommand = command;
                            daCountry.Fill(dsCountry, "Country");

                            foreach (DataRow row in dsCountry.Tables["Country"].Rows)
                            {

                                int countryId = (int)row["Id"];
                                string countryName = row["name"].ToString();
                                Country country = new Country(Guid.NewGuid(), countryName);
                                countries.Add(country);
                            }
                        }
                    }

            }
           return countries;
        }
        
    }
}
