﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Contracts;
using Timesheet.Models;


namespace Timesheet.DAL.RepositoriesDB
{
   
    public class ClientRepositoryDB : IClientRepository
    {
        //DEPENDENCY INJECTION IN WEB API(CONTROLLER)
        //UNITY

        private string _connection;
        public ClientRepositoryDB(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentException("Connection string can't be empty", nameof(connectionString));
            }
            this._connection = connectionString;
        }

        public Guid Create(Client client)
        {
            using (SqlConnection conn = new SqlConnection(_connection))
            {
                conn.Open();
                using (SqlCommand command = conn.CreateCommand())
                {

                    command.CommandText = @"INSERT INTO Client(Name,Address,City,ZipCode,@CountryId) VALUES (@Name,@Address,@City,@ZipCode,@CountryId)";
                    command.Parameters.Add(new SqlParameter("@Name", client.ClientName));
                    command.Parameters.Add(new SqlParameter("@Address", client.Address == null ? DBNull.Value : (object)client.Address));
                    command.Parameters.Add(new SqlParameter("@City", client.City == null ? DBNull.Value : (object)client.City));
                    command.Parameters.Add(new SqlParameter("@ZipCode", client.ZipCode == null ? DBNull.Value : (object)client.ZipCode));
                    command.Parameters.Add(new SqlParameter("@CountryId", client.CountryId));
                    command.ExecuteNonQuery();
                }
            }
            return client.Id;
        }


        public bool Delete(Guid id)
        {

            using (SqlConnection connection = new SqlConnection(_connection))
            {
                connection.Open();
                using (SqlCommand command = connection.CreateCommand())
                {

                    command.CommandText = @"DELETE from Client where Id= @Id";
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    command.ExecuteNonQuery();
                }
            }
            return true;
        }


        public IEnumerable<Client> GetAll()
        {
            List<Client> clients = new List<Client>();
            using (SqlConnection conn = new SqlConnection(_connection))
            {
                conn.Open();
                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = @"SELECT * FROM Client";
                    using (SqlDataAdapter daClient = new SqlDataAdapter())
                    {
                        DataSet dsClient = new DataSet();
                        daClient.SelectCommand = command;
                        daClient.Fill(dsClient, "Client");
                        foreach (DataRow row in dsClient.Tables["Client"].Rows)
                        {
                            int clientId = (int)row["Id"];
                            string clieentName = row["Name"].ToString();
                            string Address = row["Address"].ToString();
                            string City = row["City"].ToString();
                            string ZipCode = row["ZipCode"].ToString();
                            int countyId = (int)row["Id"];
                            Client c = new Client(Guid.NewGuid(), clieentName, Address, City, ZipCode, Guid.NewGuid());
                            clients.Add(c);
                        }
                    }
                }

            }
            return clients;
        }

        public Client GetById(Guid id)
        {
            using (SqlConnection conn = new SqlConnection(_connection))
            {
                conn.Open();
                using (SqlCommand command = conn.CreateCommand())
                {
                   
                    command.CommandText = @"SELECT * FROM Client WHERE Id=@Id";
                    command.Parameters.Add(new SqlParameter("@Id", id));

                    using (SqlDataAdapter daClient = new SqlDataAdapter()) {
                            DataSet dsClient = new DataSet();
                            daClient.SelectCommand = command;
                            daClient.Fill(dsClient, "Client");

                            var reader = command.ExecuteReader();

                            if(reader.HasRows)
                            {
                                    int clientId = reader.GetInt32(1);
                                    string clientName = reader.GetString(2);
                                    string Address = reader.GetString(3);
                                    string City = reader.GetString(4);
                                    string ZipCode = reader.GetString(5);
                                    int countyId = reader.GetInt32(6);

                                    Client c = new Client(Guid.NewGuid(), clientName, Address, City, ZipCode, Guid.NewGuid());
                                    return c;
                            }
                         }
                    }

                }
            return null;
        }

      


        public void Update(Guid id, Client client)
        {

            using (SqlConnection connection = new SqlConnection(_connection))
            {
                    connection.Open();
                    using (SqlCommand command = connection.CreateCommand())
                    {
                      
                        command.CommandText = @"update Client set Name=@Name,Address = @Address ,City = @City,ZipCode = @ZipCode where Id= @Id";
                        command.Parameters.Add(new SqlParameter("@Id", client.Id));
                        command.Parameters.Add(new SqlParameter("@Name", client.ClientName));
                        command.Parameters.Add(new SqlParameter("@Address", client.Address));
                        command.Parameters.Add(new SqlParameter("@City", client.City));
                        command.Parameters.Add(new SqlParameter("@ZipCode", client.ZipCode));
                        command.ExecuteNonQuery();
                    }
                }
        }


        public List<Client> FilterClients(char name)
        {
            List<Client> clients = new List<Client>();
            using (SqlConnection conn = new SqlConnection(_connection))
            {
                conn.Open();
                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = @"SELECT * FROM Client WHERE Name LIKE %?%";
                    command.Parameters.Add(new SqlParameter("@Name", name));

                    using (SqlDataAdapter daClient = new SqlDataAdapter())
                    {

                        DataSet dsClient = new DataSet();
                        daClient.SelectCommand = command;
                        daClient.Fill(dsClient, "Client");
                        foreach (DataRow row in dsClient.Tables["Client"].Rows)
                        {
                            int clientId = (int)row["Id"];
                            string clieentName = row["Name"].ToString();
                            string Address = row["Address"].ToString();
                            string City = row["City"].ToString();
                            string ZipCode = row["ZipCode"].ToString();
                            int countyId = (int)row["Id"];
                            Client c = new Client(Guid.NewGuid(), clieentName, Address, City, ZipCode, Guid.NewGuid());
                            clients.Add(c);
                        }
                    }
                }

            }
            return clients;
        }

        public List<Client> SearchClients(string name)
        {
            List<Client> clients = new List<Client>();
            using (SqlConnection conn = new SqlConnection(_connection))
            { 
                conn.Open();
                using (SqlCommand command = conn.CreateCommand())
                {
                    command.CommandText = @"SELECT * FROM Client WHERE Name = ?";
                    command.Parameters.Add(new SqlParameter("@Name", name));

                    using (SqlDataAdapter daClient = new SqlDataAdapter())
                    {

                        DataSet dsClient = new DataSet();
                        daClient.SelectCommand = command;
                        daClient.Fill(dsClient, "Client");
                        foreach (DataRow row in dsClient.Tables["Client"].Rows)
                        {
                            int clientId = (int)row["Id"];
                            string clieentName = row["Name"].ToString();
                            string Address = row["Address"].ToString();
                            string City = row["City"].ToString();
                            string ZipCode = row["ZipCode"].ToString();
                            int countyId = (int)row["Id"];
                            Client c = new Client(Guid.NewGuid(), clieentName, Address, City, ZipCode, Guid.NewGuid());
                            clients.Add(c);
                        }
                    }
                }

            }
            return clients;
        }


    }
}
