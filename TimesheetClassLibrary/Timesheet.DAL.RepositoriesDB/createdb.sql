CREATE DATABASE TimesheetDB;

USE TimesheetDB;

CREATE TABLE Country (
	Id INT PRIMARY KEY NOT NULL,
  Name VARCHAR(50) NOT NULL,
  PRIMARY KEY(Name)
);

CREATE TABLE [Client] (
	Id INT PRIMARY KEY NOT NULL,
  Name VARCHAR(50) NOT NULL,
  Address VARCHAR(50) NULL,
  City VARCHAR(50) NULL,
  ZipCode VARCHAR(50) NULL,
  CountryId  INT NOT NULL REFERENCES [Country] (Id)
  PRIMARY KEY(Name)

);


INSERT INTO [Country](name) VALUES('Serbia');
INSERT INTO [Country](name) VALUES('America');
INSERT INTO [Country](name) VALUES('Croatia');


INSERT INTO [Client]( Name,Address,City,ZipCode,CountryId) VALUES ('Marko Markovic','Novosadskog sajma','Novi Sad','21000',1);
INSERT INTO [Client]( Name,Address,City,ZipCode,CountryId) VALUES ('Petar Petrovic','Alekse Santica','Novi Sad','21000',2);
INSERT INTO [Client]( Name,Address,City,ZipCode,CountryId) VALUES ('Milen Milanovic','Novosadskog sajma','Novi Sad','21000',3);