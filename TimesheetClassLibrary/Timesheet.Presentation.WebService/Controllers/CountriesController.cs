﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Timesheet.BLL.Services;
using Timesheet.DAL.RepositoriesDB;
using Timesheet.Presentation.Contracts;
using Unity;

namespace Timesheet.Presentation.WebService.Controllers
{
   
    public class CountriesController : ApiController
    {
        
        ICountryService countryService = new CountryService(new CountryRepositoryDB(new ConnectionString().Connection));
       
      
        [HttpGet]
        [Route("api/countries")]
        public IHttpActionResult Get()
        {
            IEnumerable<ICountryDTO> countries = countryService.GetAllCountries();
            return Ok(countries);
        }

        [HttpGet]
        [Route("api/countries/{id}")]
        public IHttpActionResult GetCountryById(Guid id)
        {
            ICountryDTO country = countryService.FindCountryById(id);
            if (country == null)
            {
                return NotFound();
            }
            return Ok(country);
        }
        
    
    }
}
