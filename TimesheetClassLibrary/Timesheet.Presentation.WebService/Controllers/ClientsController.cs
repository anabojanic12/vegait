﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Timesheet.BLL.Services;
using Timesheet.DAL.RepositoriesDB;
using Timesheet.Models;
using Timesheet.Presentation.Contracts;

namespace Timesheet.Presentation.WebService.Controllers
{
    public class ClientsController : ApiController
    {
        IClientService clientService = new ClientService(new ClientRepositoryDB(new ConnectionString().Connection));


        
        [HttpGet]
        [Route("api/clients")]
        public IHttpActionResult Get()
        {
            IEnumerable<IClientEntityDTO> clients = clientService.GetAll();
            return Ok(clients);
        }

        [HttpGet]
         [Route("api/clients/{id}")]
        public IHttpActionResult GetClientById(Guid id)
        {
            IClientEntityDTO client = clientService.GetById(id);
            if (client == null)
            {
                return NotFound();
            }
            return Ok(client);
        }
        
    
        [HttpPost]
        [Route("api/clients")]
        public IHttpActionResult Create([FromBody] Client client)
        {
            try
            { 
                Guid client1 = clientService.Create(client);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Created, "Successful"));
            }
            catch (Exception e)
            {
                throw new ArgumentException();
            }
        }

        [HttpPut]
        [Route("api/clients/{id}")]
        public IHttpActionResult Update(Guid id, [FromBody] Client client)
        {
            try
            {
                clientService.Update(id, client);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NoContent, "Successful"));
            }
            catch(Exception e)
            {
                throw new ArgumentException();
            }
           

        }

        [HttpDelete]
        [Route("api/clients/{id}")]
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                clientService.Delete(id);
                return Ok();
            }
            catch(Exception e)
            {
                throw new ArgumentException();
            }
            
        }

        [HttpGet]
        [Route("api/clients/{clientName}")]
        public IHttpActionResult Filter(char clientName)
        {

            IEnumerable<IClientEntityDTO> clientsDTO = clientService.FilterClient(clientName);
            if (clientsDTO == null)
            {
                return NotFound();
            }
            return Ok(clientsDTO);
        }

        [HttpGet]
        [Route("api/clients/clients/{clientName}")]
        public IHttpActionResult SearchClients(string clientName)
        {
            IEnumerable<IClientEntityDTO> clientsDTO = clientService.SearchClients(clientName);
            if (clientsDTO == null)
            {
                return NotFound();
            }
            return Ok(clientsDTO);
        }

    }
}
