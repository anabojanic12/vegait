﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;
using Unity;

namespace Timesheet.Presentation.WebService.Unit
{
    public class UnitResolver : IDependencyResolver
    {
        protected IUnityContainer container;

        public UnitResolver(IUnityContainer unitContainer)
        {
            this.container = unitContainer ?? throw new ArgumentNullException();
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return container.Resolve(serviceType);
            } catch (Exception e)
            {
                throw new ArgumentException();
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return container.ResolveAll(serviceType);
            }catch(Exception e)
            {
                return null;
            }
        }

        public void Dispose()
        {
            container.Dispose();
        }

        public IDependencyScope BeginScope()
        {
            var child = container.CreateChildContainer();
            return new UnitResolver(child);
           
        }
    }
}