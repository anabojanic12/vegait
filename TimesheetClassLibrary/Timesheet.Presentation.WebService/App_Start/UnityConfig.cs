using System.Web.Http;
using Timesheet.BLL.Contracts;
using Timesheet.BLL.Services;
using Timesheet.DAL.Repositories;
using Timesheet.Presentation.Contracts;
using Unity;
using Unity.WebApi;

namespace Timesheet.Presentation.WebService
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);

            //Repositories
           
            container.RegisterType<IClientRepository, ClientRepository>();
            container.RegisterType<ICountryRepository, CountryRepository>();



            //Services
            container.RegisterType<IClientService, ClientService>();
            container.RegisterType<ICountryService, CountryService>();

            //Resolves dependencies
            ClientRepository clientRepository = container.Resolve<ClientRepository>();
            CountryRepository countryRepository = container.Resolve<CountryRepository>();
            ClientService clientServuce = container.Resolve<ClientService>();
            CountryService countryService = container.Resolve<CountryService>();
        }
    }
}