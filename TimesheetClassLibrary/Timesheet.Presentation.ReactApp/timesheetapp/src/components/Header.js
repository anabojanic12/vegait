import React, { Component } from 'react';
import {Link } from 'react-router-dom';
import logo from '../assets/img/logo.png';
import MobileMenu from './MobileMenu';
import {NavLink} from 'react-router-dom';

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			headerState: this.props.headerState
		};
	  } 

    render() { 
		var {headerState} = {...this.state}
        return (  
            <header className="header">
			<div className="top-bar"></div>
			<div className="wrapper">
				<a href="index.html" className="logo">
					<img src={logo} alt="VegaITSourcing Timesheet" />
				</a>
				<ul className="user right">
					<li>
						<a href="">Sladjana Miljanovic</a>
						<div className="invisible"></div>
						<div className="user-menu">
							<ul>
								<li>
									<a href="" className="link">Change password</a>
								</li>
								<li>
									<a href="" className="link">Settings</a>
								</li>
								<li>
									<a href="" className="link">Export all data</a>
								</li>
							</ul>
						</div>
					</li>
					<li className="last">
						<a href="">Logout</a>
					</li>
				</ul>
				<nav>
					<ul className="menu">
						<li>

                        {headerState === 'timesheet' ?  <NavLink to ='/timesheet' className="btn nav active"> Timesheet </NavLink> : <NavLink to ='/timesheet' className="btn nav"> Timesheet </NavLink>} 
						</li>
						<li>
                        {headerState === 'clients' ?  <NavLink to ='/clients' className="btn nav active"> Clients</NavLink> : <NavLink to ='/clients' className="btn nav"> Clients </NavLink>} 
						</li>
						<li>
                        {headerState === 'projects' ?  <NavLink to ='/projects' className="btn nav active"> Projects</NavLink> : <NavLink to ='/projects' className="btn nav"> Projects </NavLink>} 
						</li>
						<li>          
                        {headerState === 'categories' ?  <NavLink to ='/categories' className="btn nav active"> Categories</NavLink> : <NavLink to ='/categories' className="btn nav"> Categories </NavLink>} 
						</li>
						<li>
                        {headerState === 'teammembers' ?  <NavLink to ='/teammembers' className="btn nav active"> Team members</NavLink> : <NavLink to ='/teammembers' className="btn nav"> Team members</NavLink>} 
						</li>
						<li className="last">
                        {headerState === 'reports' ?  <NavLink to ='/reports' className="btn nav active"> Reports</NavLink> : <NavLink to ='/reports' className="btn nav"> Reports</NavLink>} 
						</li>
					</ul>
					<MobileMenu></MobileMenu>
								
					<span className="line"></span>
				</nav>
			</div>
		</header>
        );
    }
}
 
export default Header;