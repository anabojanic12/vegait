import React, { Component } from 'react';


class Create extends React.Component {
    constructor(props) {
        super(props);
        
		this.state = {
            createState : this.props.createState,
            client: this.props.createClient,
           
        };
	  } 

    render() {
        var createState = {...this.state.createState}
        return (  
            <div className="create"> 
            {this.props.createState === 'client' ?  

            <div className="grey-box-wrap reports">
                <a href="#new-member" className="link new-member-popup">Create new client</a>
                    <div className="search-page">
                        <input type="search" name="search-clients" className="in-search" />
                    </div>
            </div> 
            :  
            <div className="grey-box-wrap reports"> 
                <a href="#new-member" className="link new-member-popup">Create new project</a> 
                    <div className="search-page">
                        <input type="search" name="search-clients" className="in-search" /> 
                        </div>    
                    </div>
            }
           </div>
        
        );
    }
}
 
export default Create;