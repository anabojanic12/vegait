import React, { Component } from 'react';


class Details extends Component {
    
    render() {
        return ( 
            <div className="details">
                <div className="accordion-wrap clients">
					<div className="item">
						<div className="heading">
							<span>ADAM Software NV</span>
							<i>+</i>
						</div>
						<div className="details">
							<ul className="form">
								<li>
									<label>Client name:</label>
									<input type="text" className="in-text" />
								</li>
								<li>
									<label>Zip/Postal code:</label>
									<input type="text" className="in-text" />
								</li>
							</ul>
							<ul className="form">
								<li>
									<label>Address:</label>
									<input type="text" className="in-text" />
								</li>
								<li>
									<label>Country:</label>
									<select>
										<option>Select country</option>
									</select>
								</li>
							</ul>
							<ul className="form last">
								<li>
									<label>City:</label>
									<input type="text" className="in-text" />
								</li>
							</ul>
							<div className="buttons">
								<div className="inner">
									<a href="" className="btn green">Save</a>
									<a href="" className="btn red">Delete</a>
								</div>
							</div>
						</div>
					</div>


					<div className="item">
						<div className="heading">
							<span>Clockwork</span>
							<i>+</i>
						</div>
						<div className="details">
							<ul className="form">
								<li>
									<label>Client name:</label>
									<input type="text" className="in-text" />
								</li>								
								<li>
									<label>Zip/Postal code:</label>
									<input type="text" className="in-text" />
								</li>
							</ul>
							<ul className="form">
								<li>
									<label>Address:</label>
									<input type="text" className="in-text" />
								</li>
								<li>
									<label>Country:</label>
									<select>
										<option>Select country</option>
									</select>
								</li>								
							</ul>
							<ul className="form last">
								<li>
									<label>City:</label>
									<input type="text" className="in-text" />
								</li>
							</ul>
							<div className="buttons">
								<div className="inner">
									<a href="" className="btn green">Save</a>
									<a href="" className="btn red">Delete</a>
								</div>
							</div>
						</div>
					</div>
					<div className="item">
						<div className="heading">
							<span>Emperor Design</span>
							<i>+</i>
						</div>
						<div className="details">
							<ul className="form">
								<li>
									<label>Client name:</label>
									<input type="text" className="in-text" />
								</li>								
								<li>
									<label>Zip/Postal code:</label>
									<input type="text" className="in-text" />
								</li>
							</ul>
							<ul className="form">
								<li>
									<label>Address:</label>
									<input type="text" className="in-text" />
								</li>
								<li>
									<label>Country:</label>
									<select>
										<option>Select country</option>
									</select>
								</li>								
							</ul>
							<ul className="form last">
								<li>
									<label>City:</label>
									<input type="text" className="in-text" />
								</li>
							</ul>
							<div className="buttons">
								<div className="inner">
									<a href="" className="btn green">Save</a>
									<a href="" className="btn red">Delete</a>
								</div>
							</div>
						</div>
					</div>
				</div>
           </div>
         );
    }
}
 
export default Details;