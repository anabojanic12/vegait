import React, { Component } from 'react';
import logo from '../assets/img/logo.png';
import Footer from './Footer';
import Header from './Header';
import Create from './Create';
import Filter from './Filter';
import Pagination from './Pagination';
import Details from './Details';
import Modal from './Modal';
import axios from 'axios';

class Clients extends React.Component {
    constructor(props) {
		super(props);
		this.state = {
			headerState : "clients",
			createState: "client",
			details : "client",
			clients : []
		};
	  } 

	  componentDidMount() {
		axios.get('http://localhost:8874/api/clients').then(response => {
			console.log(response.clients);
			console.log("Request");
			{/*this.setState(
				clients = response.data
			) */}
		})
	  }

    render() { 
        return (
            <div className="container">
           <Header headerState = {this.state.headerState}></Header>
			<div className="wrapper">

			<section className="content">
				<h2><i className="ico clients"></i>Clients</h2>
                {/*Create client */}
				<Create createState = {this.state.createState}></Create>
				{/*Modal */}
				<Modal/>
				{/*Alphabet */}
				<Filter/>
				{/*Details - Clients*/}
				<Details/>
				{/*Pagination*/}
				<Pagination/>
			</section>		
        </div>
       <Footer/>
    </div>
    );
    }
}
 
export default Clients;