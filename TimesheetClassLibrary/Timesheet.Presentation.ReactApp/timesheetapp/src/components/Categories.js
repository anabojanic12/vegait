import React, { Component } from 'react';
import logo from '../assets/img/logo.png';
import {Link } from 'react-router-dom';
import Header from './Header';
import Footer from './Footer';

class Categories extends React.Component {
	constructor(props) {
		super(props);
		this.state = {headerState: "categories"};
	  } 
    render() { 
        return (
        <div className="container">
		 <Header headerState = {this.state.headerState}></Header>
		<div className="wrapper">
			<section className="content">
				<h2><i className="ico categories"></i>Categories</h2>
			</section>
		</div>
		<Footer></Footer>
	</div>

          );
    }
}
 
export default Categories;
