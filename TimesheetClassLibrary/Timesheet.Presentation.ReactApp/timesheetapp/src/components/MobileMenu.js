import React, { Component } from 'react';


class MobileMenu extends Component {
    state = {  }
    render() { 
        return (
            <div className="mobile-menu">
            <a href="" className="menu-btn">
                <i className="zmdi zmdi-menu"></i>
            </a>
            <ul>
                <li>
                    <a href="">TimeSheet</a>
                </li>
                <li>
                    <a href="">Clients</a>
                </li>
                <li>
                    <a href="">Projects</a>
                </li>
                <li>
                    <a href="">Categories</a>
                </li>
                <li>
                    <a href="">Team members</a>
                </li>
                <li className="last">
                    <a href="">Reports</a>
                </li>
            </ul>
            </div>
        );
    }
}
 
export default MobileMenu;

