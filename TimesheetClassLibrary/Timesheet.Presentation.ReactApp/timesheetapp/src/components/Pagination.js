import React, { Component } from 'react';

class Paginatation extends Component {
    render() { 
        return (  
            <div className="pagination">
					<ul>
						<li>
							<a href="">1</a>
						</li>
						<li>
							<a href="">2</a>
						</li>
						<li>
							<a href="">3</a>
						</li>
						<li className="last">
							<a href="">Next</a>
						</li>
					</ul>
			</div>
        );
    }
}
 
export default Paginatation;