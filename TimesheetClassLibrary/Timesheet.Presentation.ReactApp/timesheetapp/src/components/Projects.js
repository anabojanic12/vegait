import React, { Component } from 'react';
import logo from '../assets/img/logo.png';
import {Link } from 'react-router-dom';
import Footer from './Footer';
import Header from './Header';
import Create from './Create';
import Filter from './Filter';
import Details from './Details';


class Projects extends React.Component {

    constructor(props) {
		super(props);
		this.state = {
            headerState : "projects",
            createState: "project",
            details : "project"    
        };
	  } 
    
    render() { 
        return ( 
            <div className="container">
               <Header headerState = {this.state.headerState}></Header>
            
                <div className="wrapper">
                    <section className="content">
                        <h2><i className="ico projects"></i>Projects</h2>
                        {/*Create Projects */}
                        <Create createState = {this.state.createState}></Create>
                        {/**Modal*/}
                        <div className="new-member-wrap">
                            <div id="new-member" className="new-member-inner">
                                <h2>Create new project</h2>
                                <ul className="form">
                                    <li>
                                        <label>Project name:</label>
                                        <input type="text" className="in-text" />
                                    </li>								
                                    <li>
                                        <label>Description:</label>
                                        <input type="text" className="in-text" />
                                    </li>
                                    <li>
                                        <label>Customer:</label>
                                        <select>
                                            <option>Select customer</option>
                                            <option>Adam Software NV</option>
                                            <option>Clockwork</option>
                                            <option>Emperor Design</option>
                                        </select>
                                    </li>
                                    <li>
                                        <label>Lead:</label>
                                        <select>
                                            <option>Select lead</option>
                                            <option>Sasa Popovic</option>
                                            <option>Sladjana Miljanovic</option>
                                        </select>
                                    </li>
                                </ul>
                                <div className="buttons">
                                    <div className="inner">
                                        <a href="" className="btn green">Save</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        	{/*Alphabet */}
                       <Filter></Filter>
                        {/*Details - Projects */}
                        <div className="accordion-wrap projects">
                            <div className="item">
                                <div className="heading">
                                    <span>BuzzMonitor</span> <span><em>(Nina Media)</em></span>
                                    <i>+</i>
                                </div>
                                <div classame="details">
                                    <ul className="form">
                                        <li>
                                            <label>Project name:</label>
                                            <input type="text" className="in-text" />
                                        </li>
                                        <li>
                                            <label>Lead:</label>
                                            <select>
                                                <option>Select lead</option>
                                                <option>Sasa Popovic</option>
                                                <option>Sladjana Miljanovic</option>
                                            </select>
                                        </li>
                                    </ul>
                                    <ul className="form">
                                        <li>
                                            <label>Description:</label>
                                            <input type="text" className="in-text" />
                                        </li>
                                        
                                    </ul>
                                    <ul className="form last">
                                        <li>
                                            <label>Customer:</label>
                                            <select>
                                                <option>Select customer</option>
                                                <option>Adam Software NV</option>
                                                <option>Clockwork</option>
                                                <option>Emperor Design</option>
                                            </select>
                                        </li>
                                        <li className="inline">
                                        <label>Status:</label>
                                        <span className="radio">
                                            <label htmlFor="inactive">Active:</label>
                                            <input type="radio" value="1" name="status" id="inactive" />
                                        </span>
                                        <span className="radio">
                                            <label htmlFor="active">Inactive:</label>
                                            <input type="radio" value="2" name="status" id="active" />
                                        </span>
                                        <span className="radio">
                                            <label htmlFor="active">Archive:</label>
                                            <input type="radio" value="3" name="status" id="active" />
                                        </span>
                                    </li>
                                    </ul>
                                    <div className="buttons">
                                        <div className="inner">
                                            <a href="" className="btn green">Save</a>
                                            <a href="" className="btn red">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="item">
                                <div className= "heading">
                                    <span>PWN</span> <span><em>(Clockwork)</em></span>
                                    <i>+</i>
                                </div>
                                <div className="details">
                                    <ul className="form">
                                        <li>
                                            <label>Client name:</label>
                                            <input type="text" className="in-text" />
                                        </li>								
                                        <li>
                                            <label>Zip/Postal code:</label>
                                            <input type="text" className="in-text" />
                                        </li>
                                    </ul>
                                    <ul className="form">
                                        <li>
                                            <label>Address:</label>
                                            <input type="text" className="in-text" />
                                        </li>
                                        <li>
                                            <label>Country:</label>
                                            <select>
                                                <option>Select country</option>
                                            </select>
                                        </li>								
                                    </ul>
                                    <ul className="form last">
                                        <li>
                                            <label>City:</label>
                                            <input type="text" className="in-text" />
                                        </li>
                                    </ul>
                                    <div className="buttons">
                                        <div className="inner">
                                            <a href="" className="btn green">Save</a>
                                            <a href="" className="btn red">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="item">
                                <div className="heading">
                                    <span>B&G</span> <span><em>(Cubeworks)</em></span>
                                    <i>+</i>
                                </div>
                                <div className="details">
                                    <ul className="form">
                                        <li>
                                            <label>Client name:</label>
                                            <input type="text" className="in-text" />
                                        </li>								
                                        <li>
                                            <label>Zip/Postal code:</label>
                                            <input type="text" className="in-text" />
                                        </li>
                                    </ul>
                                    <ul className="form">
                                        <li>
                                            <label>Address:</label>
                                            <input type="text" className="in-text" />
                                        </li>
                                        <li>
                                            <label>Country:</label>
                                            <select>
                                                <option>Select country</option>
                                            </select>
                                        </li>								
                                    </ul>
                                    <ul className="form last">
                                        <li>
                                            <label>City:</label>
                                            <input type="text" className="in-text" />
                                        </li>
                                    </ul>
                                    <div className="buttons">
                                        <div className="inner">
                                            <a href="" className="btn green">Save</a>
                                            <a href="" className="btn red">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </section> 		
                </div>
                <Footer></Footer>
            </div>
           
         );
    }
}
 
export default Projects;