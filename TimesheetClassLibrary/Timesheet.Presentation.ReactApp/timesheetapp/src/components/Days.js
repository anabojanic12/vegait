import React, { Component } from 'react';
import Footer from './Footer';
import Header from './Header';
import {NavLink} from 'react-router-dom';

class Days extends React.Component {
    constructor(props) {
        super(props);
        this.state = {headerState: "timesheet"};
      } 

    render() { 
        return (  
            <div className="container">
                 <Header headerState = {this.state.headerState}></Header>
                <div className="wrapper">
                    <section className="content">
                        <h2><i className="ico timesheet"></i>TimeSheet</h2>
                        <div className="grey-box-wrap">
                            <div className="top">
                                <a href="" className="prev"><i className="zmdi zmdi-chevron-left"></i>previous week</a>
                                <span className="center">February 04 - February 10, 2013 (week 6)</span>
                                <a href="" className="next">next week<i className="zmdi zmdi-chevron-right"></i></a>
                            </div>
                            <div className="bottom">
                                <ul className="days">
                                    <li>
                                        <a href="">
                                            <b>Feb 04</b>
                                            <span>monday</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <b>Feb 06</b>
                                            <span>tuesday</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <b>Feb 06</b>
                                            
                                            <span>wednesday</span>
                                        </a>
                                    </li>
                                    <li className="active">
                                        <a href="">
                                            <b>Feb 07</b>
                                            
                                            <span>thursday</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <b>Feb 08</b>
                                        
                                            <span>friday</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <b>Feb 09</b>
                                        
                                            <span>saturday</span>
                                        </a>
                                    </li>
                                    <li className="last">
                                        <a href="">
                                            <b>Feb 10</b>
                                            
                                            <span>sunday</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <table className="default-table">
                        <thead>
                            <tr>
                                <th>
                                    Client <em>*</em>
                                </th>
                                <th>
                                    Project <em>*</em>
                                </th>
                                <th>
                                    Category <em>*</em>
                                </th>
                                <th>Description</th>
                                <th className="small">
                                    Time <em>*</em>
                                </th>
                                <th className="small">Overtime</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <select>
                                        <option>Choose client</option>
                                        <option>Client 1</option>
                                        <option>Client 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose project</option>
                                        <option>Project 1</option>
                                        <option>Project 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose category</option>
                                        <option>Front-End Development</option>
                                        <option>Design</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" className="in-text medium" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Choose client</option>
                                        <option>Client 1</option>
                                        <option>Client 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose project</option>
                                        <option>Project 1</option>
                                        <option>Project 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose category</option>
                                        <option>Front-End Development</option>
                                        <option>Design</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" className="in-text medium" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                                <td className ="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Choose client</option>
                                        <option>Client 1</option>
                                        <option>Client 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose project</option>
                                        <option>Project 1</option>
                                        <option>Project 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose category</option>
                                        <option>Front-End Development</option>
                                        <option>Design</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" className="in-text medium" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Choose client</option>
                                        <option>Client 1</option>
                                        <option>Client 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose project</option>
                                        <option>Project 1</option>
                                        <option>Project 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose category</option>
                                        <option>Front-End Development</option>
                                        <option>Design</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" className="in-text medium" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Choose client</option>
                                        <option>Client 1</option>
                                        <option>Client 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose project</option>
                                        <option>Project 1</option>
                                        <option>Project 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose category</option>
                                        <option>Front-End Development</option>
                                        <option>Design</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" className="in-text medium" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Choose client</option>
                                        <option>Client 1</option>
                                        <option>Client 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose project</option>
                                        <option>Project 1</option>
                                        <option>Project 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose category</option>
                                        <option>Front-End Development</option>
                                        <option>Design</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" className="in-text medium" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Choose client</option>
                                        <option>Client 1</option>
                                        <option>Client 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose project</option>
                                        <option>Project 1</option>
                                        <option>Project 2</option>
                                    </select>
                                </td>
                                <td>
                                    <select>
                                        <option>Choose category</option>
                                        <option>Front-End Development</option>
                                        <option>Design</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" className="in-text medium" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>
                                <td className="small">
                                    <input type="text" className="in-text xsmall" />
                                </td>

                            </tr>
                            </tbody>
                        </table>

                        <div className="total">
                            <a href=""><i></i>back to monthly view</a>
                            <span>Total hours: <em>7.5</em></span>
                        </div>
                    </section>			
		    </div>
		<Footer></Footer>
	</div>

        );
    }
}
 
export default Days;