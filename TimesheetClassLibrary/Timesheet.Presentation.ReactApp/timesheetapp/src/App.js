import React, { Component } from 'react';
import '../src/assets/css/style.css';
import Clients from './components/Clients';
import Projects from './components/Projects';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'; 
import TimeSheet from './components/TimeSheet';
import Categories from './components/Categories';
import TeamMembers from './components/TeamMembers';
import Reports from './components/Reports';
import Days from './components/Days';



class App extends Component {
  render() { 
    return (
      <Router> 
        <div className="container">
          <Switch>
            <Route exact path="/" component={TimeSheet} />
            <Route path="/timesheet" component={TimeSheet}></Route>
            <Route path="/projects" component={Projects}></Route>
            <Route path="/clients" component={Clients}></Route>
            <Route path="/categories" component={Categories}></Route>
            <Route path="/teammembers" component={TeamMembers}></Route>
            <Route path="/reports" component={Reports}></Route>
            <Route path="/days/date" component={Days}></Route>
          </Switch>
       </div>
      </Router>
     );
      
  }
}
 
export default App;


