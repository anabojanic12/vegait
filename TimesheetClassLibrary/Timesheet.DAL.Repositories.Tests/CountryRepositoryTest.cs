﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Contracts;
using Timesheet.BLL.Services;
using Timesheet.Models;
using Timesheet.Presentation.Contracts;

namespace Timesheet.DAL.Repositories.Tests
{
    [TestClass()]
    public class CountryRepositoryTest
    {
        [TestMethod()]
        public void  GetAllCountriesRepository_WithAllCountries_AllCountries()
        {
           ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
           Assert.IsNotNull(countryRepository.GetAll());
        }

        [TestMethod()]
        public void FindCountryById_WithIncorrentId_Null()
        {
            ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
           // Assert.IsNull(countryRepository.FindCountryById(Guid.NewGuid()));

        }

        [TestMethod()]
        public void FindCountryById_WithCorrectId_Null()
        {
            IEnumerable<ICountryDTO> countries = new List<Country>();
            ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
            countries = countryRepository.GetAll();
            Assert.IsNotNull(countryRepository.FindCountryById(countries.ElementAt(1).Id));
        }

        [TestMethod()]
        public void FindCountryById_WithEmptyId_Null()
        {

            IEnumerable<ICountryDTO> countries = new List<Country>();
            ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
            countries = countryRepository.GetAll();
           // Assert.IsNull(countryRepository.FindCountryById(Guid.Empty));
        }

    }
}
