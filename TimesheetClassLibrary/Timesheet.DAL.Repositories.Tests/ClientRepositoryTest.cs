﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Timesheet.BLL.Contracts;
using Timesheet.BLL.Services;
using Timesheet.Models;
using Timesheet.Presentation.Contracts;
using Timesheet.DAL.RepositoriesDB;
using System.Configuration;

namespace Timesheet.DAL.Repositories.Tests
{
    [TestClass()]
    public class ClientRepositoryTest
    {

        [TestMethod()]
        public void GetAllClients_ClientsList_AllClients()
        {
            using (TransactionScope transactionScope = new TransactionScope())
            {
                IClientRepository clientRepository = Substitute.For<IClientRepository>();
                Assert.IsNotNull(clientRepository.GetAll());
            } 
        }


        [TestMethod()]
        public void FindClientById_WithCorrectClientId_Null()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            IEnumerable<IClientEntityDTO> clients = new List<Client>();
            clients = clientRepository.GetAll();
            Assert.IsNotNull(clientRepository.GetById(clients.ElementAt(1).Id));
        }


        [TestMethod()]
        public void FindClientById_WithIncorrectClientId_Null()
        {

            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            Assert.IsNull(clientRepository.GetById(Guid.NewGuid()));
        }



        [TestMethod()]
        public void FindClientById_WithEmptyId_Null()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            Assert.IsNull(clientRepository.GetById(Guid.Empty));

        }


        [TestMethod()]
        public void DeleteClientById_WithClientId_CheckIfUpdated()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            Assert.IsFalse(clientRepository.Delete(Guid.NewGuid()));
        }

        [TestMethod()]
        public void SearchClients_WithClientName_AllClients()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            Assert.IsNotNull(clientRepository.SearchClients("M"));
        }


        [TestMethod()]
        public void SearchClients_WithEmptyClientName_AllClients()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            clientRepository.SearchClients(null).Returns(new List<Client>());
            Assert.IsNull(clientRepository.SearchClients("").Any());
        }


        [TestMethod()]
        public void FilterClients_WithClientFirstLetter_AllClients()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            Assert.IsNotNull(clientRepository.FilterClients('M'));
        }

        [TestMethod()]
        public void UpdateClient_WithClientNameAndClientGuid_void()
        {

            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            IClientDTO client = new Client(Guid.NewGuid(), "NewClient");
            Guid guid = clientService.Create(client);
            var newClient = clientService.GetById(guid);
            //clientService.Update(guid, newClient);
            //Assert.AreEqual(newClient.Id,updatedClient.Id);
        }
    }
}
