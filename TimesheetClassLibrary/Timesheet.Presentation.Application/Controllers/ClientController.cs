﻿using System;using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Timesheet.BLL.Contracts;
using Timesheet.BLL.Services;
using Timesheet.DAL.RepositoriesDB;
using Timesheet.Models;
using Timesheet.Presentation.Contracts;

namespace Timesheet.Presentation.Application.Controllers
{
    public class ClientController : ApiController
    {
        IClientService clientService = new ClientService(new ClientRepositoryDB(new ConnectionString().Connection));

        [HttpGet]
        public IHttpActionResult GetClients()
        {
            IEnumerable<IClientEntityDTO> clients = clientService.GetAll();
            if (clients == null)
            {
                return NotFound();
            }
            return Ok(clients);
        }

        [HttpGet]
        public IHttpActionResult GetClientById(Guid id)
        {
            IClientEntityDTO client = clientService.GetById(id);
            if (client == null)
            {
                return NotFound();
            }
            return Ok(client);
        }
    
        [HttpPost]
        public IHttpActionResult Create([FromBody]IClientDTO client)
        {
             try
             {
                 var client1 = clientService.Create(client);
                 return Ok(client1);
             }catch(Exception e)
             {
                  return NotFound();
             }
        }

        [HttpPut]
        public IHttpActionResult Update(Guid id,[FromBody]IClientDTO client)
        {
            IClientEntityDTO updateClient = clientService.GetById(id);
            if(updateClient == null)
            {
                return NotFound();
            }
            clientService.Update(id, client);
            return Ok(updateClient);  
        }

        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            IClientEntityDTO client = clientService.GetById(id);
             if (client == null)
             {
                return NotFound();
             }
            return Ok();
        }

        [HttpGet]
        public IHttpActionResult Filter(char clientName) {

            IEnumerable<IClientEntityDTO> clientsDTO = clientService.FilterClient(clientName);
            if (clientsDTO == null)
            {
                return NotFound();
            }
            return Ok(clientsDTO);
        }

        [HttpGet]
        public IHttpActionResult SearchClients(string clientName)
        {
            IEnumerable<IClientEntityDTO> clientsDTO = clientService.SearchClients(clientName);
            if (clientsDTO == null)
            {
                return NotFound();
            }
            return Ok(clientsDTO);
        }
    }
}

