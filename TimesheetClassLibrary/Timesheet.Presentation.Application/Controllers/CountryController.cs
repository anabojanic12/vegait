﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Timesheet.BLL.Contracts;
using Timesheet.BLL.Services;
using Timesheet.DAL.RepositoriesDB;
using Timesheet.Models;
using Timesheet.Presentation.Contracts;

namespace Timesheet.Presentation.Application.Controllers
{
    public class CountryController : ApiController
    {
        ICountryService countryService = new CountryService(new CountryRepositoryDB(new ConnectionString().Connection));

        [HttpGet]
        public IHttpActionResult GetCountries()
        {
            IEnumerable<ICountryDTO> countries = countryService.GetAllCountries();
            if (countries == null)
            {
                return NotFound();
            }
            return Ok(countries);
        }

        [HttpGet]
        public IHttpActionResult GetCountryById(Guid id)
        {
            ICountryDTO country = countryService.FindCountryById(id);
            if(country == null)
            {
                return NotFound();
            }
            return Ok(country);
        }



    }

}
