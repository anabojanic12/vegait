﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.Presentation.Contracts;

namespace Timesheet.Models
{
    public class Country : ICountryDTO
    {
        public Guid Id { get; set; }
        private string _countryName;

        public string CountryName
        {
            get { return _countryName; }
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Name can't be empty",nameof(_countryName));
                }

                _countryName = value;
            }
        }

        public Country(Guid Id,string CountryName) 
        {

            this.Id = Id;
            this.CountryName = CountryName;

        }  


        public override string ToString()
        {
            return "Id " + Id + " CountryName " + CountryName;
        } 


    }
}
