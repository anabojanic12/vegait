﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.Presentation.Contracts;

namespace Timesheet.Models
{
    public class Client : IClientEntityDTO
    {
        public Guid Id { get; set; }

        private string _clientName;

        public string ClientName
        {
            get { return _clientName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Name can't be empty", nameof(_clientName));
                }

                _clientName = value;
            }
        }

      

        public string Address { get; set; }

        public string City { get; set; }

        public string ZipCode { get; set; }

        public Guid? CountryId { get; set; }


      


        public Client(Guid Id,string ClientName)
        {

            this.Id = Id;
            this.ClientName = ClientName;

        }


        public Client(Guid Id,string ClientName,string Address,string City,string ZipCode,Guid countryId)
        {
            this.Id = Id;
            this.ClientName = ClientName;
            this.Address = Address;
            this.City = City;
            this.ZipCode = ZipCode;
            this.CountryId = CountryId;
            
        }

        public Client(Guid Id, string ClientName, string Address, string City, string ZipCode)
        {
            this.Id = Id;
            this.ClientName = ClientName;
            this.Address = Address;
            this.City = City;
            this.ZipCode = ZipCode;
           

        }


        public override string ToString()
        {
            return "Id:" + Id + " ClientName:" + ClientName + "  Address:" + Address + " City:" + City + " ZipCode:" + ZipCode;
        }


    }
}
