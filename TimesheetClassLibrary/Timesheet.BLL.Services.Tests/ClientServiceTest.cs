﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Contracts;
using Timesheet.DAL.Repositories;
using Timesheet.Models;
using Timesheet.Presentation.Contracts;

namespace Timesheet.BLL.Services.Tests
{
    [TestClass()]
    public class ClientServiceTest
    {

        //sta se testira,pod kojim uslovima se testira,sta je rezultat
       

        [TestMethod()]
        public void GetAllClients_ClientsList_AllClients()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            Assert.IsNotNull(clientService.GetAll());
        }

        [TestMethod()]
        public void FindClientById_WithCorrectClientId_Null()
        {

            IEnumerable<IClientEntityDTO> clients = new List<Client>();
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            clients = clientService.GetAll();
            Assert.IsNotNull(clientService.GetById(clients.ElementAt(1).Id));
        }

        [TestMethod()]
        public void FindClientById_WithInCorrectClientId_Null()
        {

            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            Assert.IsNull(clientService.GetById(Guid.NewGuid()));

        }


        [TestMethod()]
        public void FindClientById_WithEmptyId_Null()
        {

            IEnumerable<IClientEntityDTO> clients = new List<Client>();
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            clients = clientService.GetAll();
            Assert.IsNotNull(clientService.GetById(Guid.Empty));

        }



        [TestMethod()]
        public void CreateClient_WithNewClient_AddedClient()
        {

            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            Client newClient = new Client(Guid.NewGuid(), "Client2342134");
            Assert.IsNotNull(clientService.Create(newClient));
        }



        [TestMethod()]
        public void CreateClient_WithoutClientName_AddedClient()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            Client newClient = new Client(Guid.NewGuid(), string.Empty);
            Assert.IsNotNull(clientService.Create(newClient));
        }


        [TestMethod()]
        public void DeleteClientById_WithClientId_CheckIfUpdated()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            Assert.IsFalse(clientService.Delete(Guid.NewGuid()));
        }


        [TestMethod()]
        public void DeleteClientById_WithEmptyGuid_CheckIfUpdated()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            Assert.IsFalse(clientService.Delete(Guid.Empty));
        }





        [TestMethod()]
        public void FilterClients_WithClientFirstLetter_AllClients()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            Assert.IsNotNull(clientService.FilterClient('C'));

        }



        [TestMethod()]
        public void SearchClients_WithClientName_AllClients()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            Assert.IsNotNull(clientService.SearchClients("Client"));
        }


        [TestMethod()]
        public void SearchClients_WithEmptyClientName_AllClients()
        {
            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            clientRepository.SearchClients(null).Returns(new List<Client>());
            Assert.IsNull(clientService.SearchClients("").Any());
        }



        [TestMethod()]
        public void UpdateClient_WithClientNameAndClientGuid_void() {

            IClientRepository clientRepository = Substitute.For<IClientRepository>();
            ClientService clientService = new ClientService(clientRepository);
            IClientDTO client = new Client(Guid.NewGuid(),"NewClient");
            Guid guid = clientService.Create(client);
            var newClient = clientService.GetById(guid);
            //clientService.Update(guid, newClient);
            //Assert.AreEqual(newClient.Id,updatedClient.Id);

        }

    }
}
