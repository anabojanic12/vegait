﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Contracts;
using Timesheet.DAL.Repositories;
using Timesheet.Models;
using Timesheet.Presentation.Contracts;


namespace Timesheet.BLL.Services.Tests
{
    [TestClass()]
    public class CountryServiceTest
    {
        [TestMethod()]
        public void GetAllCountries_WithAllCountries_AllCountries()
        {

            ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
            CountryService countryService = new CountryService(countryRepository);
            Assert.IsNotNull(countryService.GetAllCountries());
        }

        [TestMethod()]
        public void FindCountryById_WithIncorrectId_Null()
        {

            ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
            CountryService countryService = new CountryService(countryRepository);
            Assert.IsNull(countryService.FindCountryById(Guid.NewGuid()));
        }


        [TestMethod()]
        public void FindCountryById_WithCorrectId_Null()
        {
            IEnumerable<ICountryDTO> countries = new List<Country>();
            ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
            CountryService countryService = new CountryService(countryRepository);
             countries = countryService.GetAllCountries();
            Assert.IsNotNull(countryService.FindCountryById(countries.ElementAt(1).Id));
        }


        [TestMethod()]
        public void FindCountryById_WithEmptyId_Null()
        {

            IEnumerable<ICountryDTO> countries = new List<Country>();
            ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
            CountryService countryService = new CountryService(countryRepository);
            countries = countryService.GetAllCountries();
            Assert.IsNotNull(countryService.FindCountryById(Guid.Empty));
        }

        [TestMethod()]
        public void TestCountryServiceConstructor_WithICountryRepository_Null()
        {


            ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
            CountryService countryService = new CountryService(countryRepository);
            Assert.IsNotNull(countryService);
        }


        [TestMethod()]
        public void TestCountryServiceConstructor_WithoutICountryRepository_Null()
        {


            ICountryRepository countryRepository = Substitute.For<ICountryRepository>();
            CountryService countryService = new CountryService(null);
            Assert.ThrowsException<ArgumentNullException>(() => countryService);

        }

    }
}
