﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Contracts;
using Timesheet.Models;

namespace Timesheet.DAL.Repositories
{
    public class ClientRepository : IClientRepository
    {
        List<Client> clients = new List<Client>();
        public ClientRepository()
        {
            clients.Add(new Client(Guid.NewGuid(), "Client1","Cara Dusana" , "Novi Sad", "210000", Guid.NewGuid()));
            clients.Add(new Client(Guid.NewGuid(), "Client2", "Cara Dusana", "Novi Sad", "210000", Guid.NewGuid()));
        }

        
        public IEnumerable<Client> GetAll()
        {
            return clients;
        }

        public Client GetById(Guid id)
        {
            foreach(Client client in GetAll())
            {
                if(client.Id == id)
                {
                    return client;
                }   
            }
            return null;
        }


        public Guid Create(Client client)
        { 
             if (CheckThatClientNameExist(client.ClientName))
             {
                 throw new ArgumentNullException("Error while creating new client");
             }

             Client newClient = new Client(client.Id, client.ClientName)
             {
                 Address = client.Address,
                 ZipCode = client.ZipCode,
                 City = client.City,
                 CountryId = client.CountryId
             };

             clients.Add(newClient);
             return newClient.Id;
        }


        public bool Delete(Guid id)
        {
            foreach (var client in clients)
            {
                if (client.Id == id)
                {
                    clients.Remove(client);
                    return true;
                }
            }
            return false;
        }


        public void Update(Guid id,Client client)
        {
            Client updatedclient = GetById(id);

            updatedclient.Id = client.Id;
            updatedclient.ClientName = client.ClientName;
            updatedclient.City = client.City;
            updatedclient.ZipCode = client.ZipCode;
            updatedclient.CountryId = client.CountryId;
           /* if (!CheckThatClientNameExist(client.ClientName))
            {
                updatedclient.Id = client.Id;
                updatedclient.ClientName = client.ClientName;
                updatedclient.City = client.City;
                updatedclient.ZipCode = client.ZipCode;
                updatedclient.CountryId = client.CountryId;
            }
            
            */
        }

        public List<Client> SearchClients(string name)
        {
            List<Client> searchedClients = new List<Client>();

            foreach (var client in clients)
            {
                if (client.ClientName.Contains(name))
                {
                    searchedClients.Add(client);
                }

               
            }
            return searchedClients;
        }

        public List<Client> FilterClients(char name)
        {
            List<Client> filteredClients = new List<Client>();

            foreach (var client in clients)
            {
                if (client.ClientName[0].Equals(char.IsUpper(name)) || client.ClientName[0].Equals(char.IsLower(name)))
                {
                    filteredClients.Add(client);
                }
            }

            return filteredClients;
        }

        private bool CheckThatClientNameExist(string clientName)
        {
            foreach (var client in clients)
            {
                if (client.ClientName.Equals(clientName))
                {
                    return true;
                }
            }
            return false;
        }

       
    }
}
