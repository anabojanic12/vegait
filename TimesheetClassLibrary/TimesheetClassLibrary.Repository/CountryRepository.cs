﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Contracts;
using Timesheet.Models;

namespace Timesheet.DAL.Repositories
{
    public class CountryRepository : ICountryRepository
    {
        List<Country> countries = new List<Country>();
        public CountryRepository()
        {
            countries.Add(new Country(Guid.NewGuid(), "Serbia"));
            countries.Add(new Country(Guid.NewGuid(), "America"));
            countries.Add(new Country(Guid.NewGuid(), "Croatia"));

        }


        public IEnumerable<Country> GetAll()
        {
            return countries;
        }


        public Country FindCountryById(Guid id)
        {
            
            foreach(var country in countries)
            {
               if(country.Id == id)
                {
                    return country;
                }
            }
            return null;
        }
    }
}