﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.Models.Tests
{
    [TestClass()]
    public class CountryTest
    {
        [TestMethod()]
        public void CheckCountryNameAndGuid_WithCountryNameAndGuid_void()
        {

            Country country = new Country(Guid.NewGuid(),"Serbia");

            Assert.AreNotEqual(string.Empty, country.CountryName);
            Assert.IsNotNull(country.CountryName);
        }

        [TestMethod()]
        public void CheckCountryConstructor_WithCountryNameAndGuid_ArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => new Country(Guid.NewGuid(),"America"));
            
        }

    }
}
