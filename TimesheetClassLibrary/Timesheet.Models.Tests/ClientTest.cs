﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.Models.Tests
{
    [TestClass()]
    public class ClientTest
    {
        [TestMethod()]
        public void CheckClientNameAndGuid_WithClientGuidAndName_void()
        {
            Client client = new Client(Guid.NewGuid(),"Client2");
            Assert.AreNotEqual(string.Empty, client.ClientName);
            Assert.IsNotNull(client.ClientName);
        }

        [TestMethod()]
        public void CheckClientConstructor_WithClientGuidAndName_ArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => new Client(Guid.NewGuid(),"Client1"));
        }
    }
}
