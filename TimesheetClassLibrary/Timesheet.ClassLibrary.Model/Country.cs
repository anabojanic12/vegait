﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Timesheet.Models
{
    public class Country :ICountryDTO
    {
        public Guid Id { get; set; }

        private string _countryName;

        public string CountryName
        {
            get { return _countryName; }
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Name can't be empty",nameof(CountryName));
                }

                _countryName = value;
            }  
        }

        public Country(Guid Id,string name)
        {

            this.Id = Id;
            this.CountryName = name;
        }

       
        public override string ToString()
        {
            return "Id " +  Id + " CountryName " + CountryName ;
        }

      
    } 
}
