﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.Models;

namespace Timesheet.BLL.Contracts
{
    public interface ICountryRepository
    {
        IEnumerable<Country> GetAll();

        Country FindCountryById(Guid id);

        
    }
}
