﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.Models;

namespace Timesheet.BLL.Contracts
{
    public interface IClientRepository
    {
        IEnumerable<Client> GetAll();

        Client GetById(Guid id);

        Guid Create(Client client);

        void Update(Guid id ,Client client);

        bool Delete(Guid id);

        List<Client> SearchClients(string name);

        List<Client> FilterClients(char name);

       


    }
}
