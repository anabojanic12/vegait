﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.Presentation.Contracts
{
    public interface IClientService
    {

        IEnumerable<IClientEntityDTO> GetAll();

        IClientEntityDTO GetById(Guid id);



        Guid Create(IClientDTO clientDTO);

        void Update(Guid id,IClientDTO clientDTO);

        bool Delete(Guid id);

        IEnumerable<IClientEntityDTO> SearchClients(string name);

        IEnumerable<IClientEntityDTO> FilterClient(char name);



    }
}
