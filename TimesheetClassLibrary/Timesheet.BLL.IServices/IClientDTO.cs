﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.Presentation.Contracts
{
    public interface IClientDTO
    {

      
         string ClientName { get; }

         string Address { get; }

         string City { get; }

         string ZipCode { get; }

         Guid? CountryId { get; }   
    }

    public interface IClientEntityDTO : IClientDTO
    {
        Guid Id { get; }
    }
}
