﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.Presentation.Contracts
{
    public interface ICountryService
    {
        IEnumerable<ICountryDTO> GetAllCountries();

        ICountryDTO FindCountryById(Guid id);
    }
}
