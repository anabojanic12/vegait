﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Contracts;
using Timesheet.Presentation.Contracts;

namespace Timesheet.BLL.Services
{
    public class CountryService : ICountryService
    {
        private readonly ICountryRepository _countryRepository;

        public CountryService(ICountryRepository icountryRepository)
        {
            _countryRepository = icountryRepository ?? throw new ArgumentNullException();
        }


        public ICountryDTO FindCountryById(Guid id)
        {
            return _countryRepository.FindCountryById(id);
        }

        public IEnumerable<ICountryDTO> GetAllCountries()
        {
            return _countryRepository.GetAll();
        }
    }
}
