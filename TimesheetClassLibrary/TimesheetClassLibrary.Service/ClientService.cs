﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Contracts;
using Timesheet.DAL.Repositories;
using Timesheet.Models;
using Timesheet.Presentation.Contracts;

namespace Timesheet.BLL.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientReposity;

        public ClientService(IClientRepository clientRepository)
        {
            _clientReposity = clientRepository ?? throw new ArgumentNullException();

        }

        public IEnumerable<IClientEntityDTO> GetAll()
        {
            return _clientReposity.GetAll();
        }

        public IClientEntityDTO GetById(Guid id)
        {

            return _clientReposity.GetById(id);
        }

        public Guid Create(IClientDTO clientDTO)
        {
            Client clientEntityDTO = new Client(Guid.NewGuid(),clientDTO.ClientName,clientDTO.Address,clientDTO.City,clientDTO.ZipCode,clientDTO.CountryId.Value);
            //Client clientEntityDTO = new Client(Guid.NewGuid(), clientDTO.ClientName, clientDTO.Address, clientDTO.City, clientDTO.ZipCode);
            return _clientReposity.Create(clientEntityDTO);
        }

        public bool Delete(Guid id)
        {
            return _clientReposity.Delete(id);
            
        }

        public IEnumerable<IClientEntityDTO> FilterClient(char name)
        {
            return _clientReposity.FilterClients(name);
          
        }
        public IEnumerable<IClientEntityDTO> SearchClients(string name)
        {
            return _clientReposity.SearchClients(name);
        }

        public void Update(Guid id, IClientDTO clientDTO)
        {
            Client clientEntityDTO = new Client(id, clientDTO.ClientName)
            {
                Address = clientDTO.Address,
                City = clientDTO.City,
                ZipCode = clientDTO.ZipCode,
                CountryId  = clientDTO.CountryId
            };

            _clientReposity.Update(id,clientEntityDTO);
        }

       

        
    }
}
