using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheet.BLL.Services;
using Timesheet.DAL.Repositories;
using Timesheet.DAL.RepositoriesDB;
using Timesheet.Models;
using Timesheet.Presentation.Contracts;

namespace Timesheet.Presentation.App
{
    class Program
    {
        static void Main(string[] args)
        {
            string choose;
            do
            {
                Console.WriteLine("Select  the number of your choice:---->  ");
                Console.WriteLine("1 - All Countries ");
                Console.WriteLine("2 - All Clients");
                Console.WriteLine("3 - Create Client");
                Console.WriteLine("4 - Find Country By Id");
                Console.WriteLine("5 - Exit");

                Console.WriteLine("Enter the number of your choice:----> ");

                choose = Console.ReadLine();
                //just example
                string connectionString = "";
                switch(choose)
                {
                    case "1":

                        CountryRepositoryDB countryRepositoryDB = new CountryRepositoryDB(connectionString);
                      
                        ICountryService countryService = new CountryService(countryRepositoryDB);
                        foreach (var count in countryService.GetAllCountries())
                        {
                            Console.WriteLine(count);
                        }
                        
                       
                        break;
                    case "2":

                        ClientRepositoryDB clientRepository = new ClientRepositoryDB(connectionString);
                        IClientService clientService = new ClientService(clientRepository); 

                        foreach (var client2 in clientService.GetAll())
                        {
                            Console.WriteLine(client2);
                        }
                        
                        break;
                    case "3":

                      

                        ClientRepositoryDB cRepository = new ClientRepositoryDB(connectionString);
                        IClientService cService = new ClientService(cRepository);
                        
                        CountryRepositoryDB countryRepo = new CountryRepositoryDB(connectionString);
                        ICountryService countryServ= new CountryService(countryRepo);

                        IEnumerable<ICountryDTO> countries = countryServ.GetAllCountries(); 

                      
                         Console.WriteLine("Insert client name");
                         string clientName = Console.ReadLine(); 

                     
                          Console.WriteLine("Insert address");
                          string address = Console.ReadLine();
                          Console.WriteLine("Insert city");
                          string city = Console.ReadLine();
                          Console.WriteLine("Insert zip code");
                          string zipCode = Console.ReadLine();

                        int counterCountry = 0;
                        Guid guid = new Guid();

                       
                        Console.WriteLine("********************All Countries********************");

                       
                        foreach(var c in countryServ.GetAllCountries())
                        {
                            counterCountry++;
                            Console.WriteLine(counterCountry + " ) " + c);
                            guid = c.Id;
                           
                        }


                        Console.WriteLine("Insert country number from the list:");
                        string countryNumber = Console.ReadLine();


                        int selectedCountry = int.Parse(countryNumber);

                        Console.WriteLine("Guid is : " + guid);
                        Console.WriteLine("Selected country is " + selectedCountry);


                        
                        var country = countryServ.FindCountryById(guid);

                        Console.WriteLine("Country: " + country);

                      
                        IClientDTO newclient = new Client(Guid.NewGuid(), clientName)
                        {
                            Address = address,
                            City = city,
                            ZipCode = zipCode,
                            CountryId = Guid.NewGuid()

                        };

                        cService.Create(newclient);

                        Console.WriteLine(newclient);
                        
    
                        break;

                    case "4":
                      

                        CountryRepositoryDB countryRepository = new CountryRepositoryDB(connectionString);
                        ICountryService countryServ1 = new CountryService(countryRepository);

                        //IEnumerable<ICountryDTO> allcountries = countryServ1.GetAllCountries();
                        int counter = 0;
                        Guid guid1 = new Guid();


                        //int number = 0;
                        Console.WriteLine("********************All Countries********************");


                        foreach (var c in countryServ1.GetAllCountries())
                        {
                            counter++;
                            Console.WriteLine(counter + " ) " + c);
                            guid1 = c.Id;
                            // guid = c.Id;

                        }
                        Console.WriteLine("Insert country number from the list:");
                        string NumberCountry = Console.ReadLine();


                        //   int selectedCountry = int.Parse(countryNumber);

                        // Console.WriteLine("Guid is : " + guid);
                        //Console.WriteLine("Selected country is " + selectedCountry);
                        var findCountrycountry = countryServ1.FindCountryById(guid1);
                        Console.WriteLine(findCountrycountry);
                        break;

                  

                    default:
                        break;
                }


            } while (Int32.Parse(choose) != 5);

        
        }
    }
}
